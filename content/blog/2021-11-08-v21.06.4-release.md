title: "postmarketOS Release: v21.06 Service Pack 4"
title-short: "v21.06 SP4"
date: 2021-11-08
preview: "2021-11/numberstation.jpg"
---

[![](/static/img/2021-11/numberstation_thumb.jpg){: class="wfull border"}](/static/img/2021-11/numberstation.jpg)

A fresh new service pack bringing some nice features from edge to stable. This
is the last service pack in the v21.06 release cycle, we plan to publish a new
postmarketOS v21.12 release in December, based on the
[upcoming Alpine 3.15](https://lists.alpinelinux.org/~alpine/devel/%3CYWsU6tiHtmsWIma7%40alpha%3E)
and a soon-to-be-created new branch from postmarketOS edge.

## Contents

* [Phosh 0.14.0](https://gitlab.gnome.org/World/Phosh/phosh/-/tags/v0.14.0)
  brings various improvements, most noteworthy
  [launch splash support](https://gitlab.gnome.org/World/Phosh/phosh/-/merge_requests/886)
  and [seek buttons](https://fosstodon.org/@ollieparanoid/107113299085932411)
  for the media player widget.

* [Numberstation 1.0.0](https://git.sr.ht/~martijnbraam/numberstation/refs/1.0.0)
  is the 2FA token app shown in the photo. With a newly added import/export
  feature to quickly backup and restore keys and move them across devices, as
  well as the newly added
  [HOTP](https://en.wikipedia.org/wiki/HMAC-based_one-time_password) support,
  it is now considered feature complete. Backup files are accepted in the
  format of one `otpauth://` URL per line. For HOTP support, keys are based on
  counters instead of time. UI changes were made to not increment this counter
  by accident.

* [eg25-manager 0.4.1](https://gitlab.com/mobian1/devices/eg25-manager), the
  helper daemon for the EG25 modem in the PinePhone, has gotten
  [GNSS assistence support](https://gitlab.com/mobian1/devices/eg25-manager/-/merge_requests/15)
  and lots of small general improvements compared to the version we had
  previously packaged on the stable branch.

* [Pixman](https://gitlab.freedesktop.org/pixman) is still at the latest
  official 0.40.0 version from April 2020 (apparently upstream doesn't release
  that often). However we cherry picked the recently merged patch to use
  [NEON instructions on ARM64 devices](https://gitlab.freedesktop.org/pixman/pixman/-/merge_requests/20)
  after having it in
  [Alpine edge](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/24706)
  for a while.
  Pixman is a low level CPU compositing and drawing library that is used in
  *lots* of applications, therefore this makes all aarch64 phones a bit more
  responsive.

## How to get it
Find the most recent images at our [download page](/download/). Existing users
of the v21.06 release will receive this service pack automatically on their
next system update. If you read this blog post right after it was published,
it may take a bit until binary packages are available.

Thanks to everybody who made this possible, especially our amazing community
members and upstream projects.

## Comments

* [Mastodon](https://fosstodon.org/@postmarketOS/107243218147737266)
* [Lemmy](https://lemmy.ml/post/88444)
<small>
* [Reddit](https://www.reddit.com/r/postmarketOS/comments/qpla36/postmarketos_release_v2106_service_pack_4/)
* [HN](https://news.ycombinator.com/item?id=29153307)
</small>
