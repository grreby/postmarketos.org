title: "Samsung Galaxy S III (samsung-m0): Phosh crashes"
date: 2022-08-31
---

Since [aports!35027](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/35027)
Phosh is crashing on samsung-m0. This is being analyzed in
[phosh#828](https://gitlab.gnome.org/World/Phosh/phosh/-/issues/828).
