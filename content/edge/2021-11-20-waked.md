title: "waked 0.1.1-r0 caused hanging at boot"
date: 2021-11-20
---

There was briefly a waked 0.1.1-r0 package, which caused hanging at boot. The
bug has been fixed in waked 0.1.1-r1, 
[aports!27630](https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/27630).
Make sure you didn't install the broken version before rebooting.
