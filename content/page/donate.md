title: "Donate to postmarketOS"
---
While very much appreciated, we decided not to accept small donations anymore.
Instead we suggest you have a look at our [merch page](/merch). When buying
anything from there, you make a small donation to postmarketOS (and the best
part is, you get something in return!).

If you would like to make a big donation, contact us via
donation@<span style="display:none">
</span>postmarketos<span style="display:none">
</span>.org for the SEPA details and other possible payment methods.
Depending on your region, the donation may be tax deductibe.

In any case, thank you for considering a donation to postmarketOS!
